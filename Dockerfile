FROM debian:stable-slim AS base

RUN apt-get update && \
    apt-get install -y texlive-latex-base && \
    apt-get clean

WORKDIR /src
CMD bash

FROM base AS default
RUN apt-get update && \
    apt-get install -y texlive && \
    apt-get clean

FROM default AS full
RUN apt-get update && \
    apt-get install -y texlive-full && \
    apt-get clean
